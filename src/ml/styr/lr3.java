package ml.styr;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class lr3 {

    public static void main(String args[]) {

        Random rnd = new Random();
        int arrSize = 10;

        System.out.println("\n task 1 - is array sorted?");

        int arrayInc[] = {0, 2, 4, 5, 6};
        int arrayDec[] = {9, 8, 4, 3, 2};
        int arrayRand[] = {0, 2, 4, 9, 5};

        System.out.print(Arrays.toString(arrayInc) + " -- ");
        testArray(arrayInc);
        System.out.print(Arrays.toString(arrayDec) + " -- ");
        testArray(arrayDec);
        System.out.print(Arrays.toString(arrayRand) + " -- ");
        testArray(arrayRand);

        System.out.println("\n task 2 - insert sort");

        int array2sort[] = new int[arrSize];
        for (int i = 0; i < arrSize; i++) {
            array2sort[i] = rnd.nextInt(50) - 10;
        }
        System.out.println("Array before sort: " + Arrays.toString(array2sort));
        System.out.println(" Array after sort: " + Arrays.toString(insertSort(array2sort)));

        System.out.println("\n task 3 - merge two sorted arrays");
        int arr1[] = {1, 3, 6, 8, 9, 11};
        int arr2[] = {1, 2, 5, 6, 15};
        System.out.println("Arr1: " + Arrays.toString(arr1));
        System.out.println("Arr2: " + Arrays.toString(arr2));
        System.out.println("Merged array: " + Arrays.toString(mergeArrays(arr1, arr2)));

        int arr3[] = {11, 9, 8, 6, 3, 1};
        int arr4[] = {15, 6, 5, 2, 1};
        System.out.println("Arr1: " + Arrays.toString(arr3));
        System.out.println("Arr2: " + Arrays.toString(arr4));
        System.out.println("Merged array: " + Arrays.toString(mergeArrays(arr3, arr4)));

        int arr5[] = {1, 3, 6, 8, 9, 11};
        int arr6[] = {15, 6, 5, 2, 1};
        System.out.println("Arr1: " + Arrays.toString(arr5));
        System.out.println("Arr2: " + Arrays.toString(arr6));
        System.out.println("Merged array: " + Arrays.toString(mergeArrays(arr5, arr6)));

        System.out.println("\n task 4 - merge sort");
        int randArr1[] = new int[arrSize];
        int randArr2[] = new int[arrSize];

        for (int i = 0; i < arrSize; i++) {
            randArr1[i] = rnd.nextInt(100) - 50;
            randArr2[i] = rnd.nextInt(6);
        }

        System.out.println("Rand array 1 : " + Arrays.toString(randArr1));
        System.out.println("Rand array 2 : " + Arrays.toString(randArr2));
        System.out.println("Merged array: " + Arrays.toString(mergeSort(randArr1, randArr2)));

        System.out.println("\n task 5 - binary search");

        System.out.println("Search for 5 in array " + Arrays.toString(randArr2));
        int searchResult = binarySearch0(randArr2, 0, randArr2.length - 1, 5);
        System.out.println("5 is " + (searchResult < 0 ? "not in array" : "at " + (searchResult + 1) + " position"));


        System.out.println("\n Individual task 1 - Дано масив дійсних чисел. Створити новий масив, який не містить дублікатів");
        individual1();

        System.out.println("\n Individual task 2 - У матриці дійсних чисел NxN знайти максимальний і мінімальний елемент у заданому стовпчику");
        individual2();




    }

    private static void testArray(int arr[]) {
        int i = 1;
        while (i < arr.length - 1) {
            if (arr[i - 1] < arr[i] && arr[i] < arr[i + 1]) {
                if (i + 2 == arr.length) {
                    System.out.println("Increment Array");
                    break;
                }
            } else if (arr[i - 1] > arr[i] && arr[i] > arr[i + 1]) {
                if (i + 2 == arr.length) {
                    System.out.println("Decrement Array");
                    break;
                }
            } else {
                System.out.println("Array is not sorted!");
                break;
            }
            i++;
        }
    }

    private static int[] insertSort(int arr[]) {
        int tmp, j;
        for (int i = 0; i < arr.length; i++) {
            tmp = arr[i];
            for (j = i - 1; j >= 0 && arr[j] > tmp; j--) {
                arr[j + 1] = arr[j];
            }
            arr[j + 1] = tmp;
        }
        return arr;
    }

    private static int[] mergeArrays(int arr1[], int arr2[]) {
        int i = 0, i1 = 0, i2 = 0;
        int arr[] = new int[arr1.length + arr2.length];
        if (arr1[0] < arr1[arr1.length - 1] && arr2[0] < arr2[arr2.length - 1]) {
            while (i < arr.length - 1)
                arr[i++] = arr1[i1] > arr2[i2] ? arr2[i2 < arr2.length - 1 ? i2++ : i2] : arr1[i1 < arr1.length - 1 ? i1++ : i1];
            arr[i] = arr1[i1] < arr2[i2] ? arr2[i2] : arr1[i1];
        } else {
            if (arr1[0] > arr1[arr1.length - 1] && arr2[0] > arr2[arr2.length - 1]) {
                while (i < arr.length - 1)
                    arr[i++] = arr1[i1] < arr2[i2] ? arr2[i2 < arr2.length - 1 ? i2++ : i2] : arr1[i1 < arr1.length - 1 ? i1++ : i1];
                arr[i] = arr1[i1] > arr2[i2] ? arr2[i2] : arr1[i1];
            } else {
                i2 = arr2.length - 1;
                while (i < arr.length - 1)
                    arr[i++] = arr1[i1] > arr2[i2] ? arr2[i2 >= 0 ? i2-- : i2] : arr1[i1 < arr1.length - 1 ? i1++ : i1];
                arr[i] = arr1[i1] < arr2[i2] ? arr2[i2] : arr1[i1];
            }
        }

        return arr;
    }

    private static int[] mergeSort(int arr1[], int arr2[]) {
        System.out.println(Arrays.toString(insertSort(arr1)));
        System.out.println(Arrays.toString(insertSort(arr2)));
        int arr[] = mergeArrays(insertSort(arr1), insertSort(arr2));
        System.out.println(Arrays.toString(insertSort(arr)));
        return arr;
    }

    private static int binarySearch0(int[] a, int fromIndex, int toIndex, int key) {
        int low = fromIndex;
        int high = toIndex - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal < key)
                low = mid + 1;
            else if (midVal > key)
                high = mid - 1;
            else
                return mid;
        }
        return -(low + 1);
    }

    private static void individual1() {
        double array[] = {1.0, 1.0, 1.2, 1.3, 1.5, 1.5, 1.7, 1.8, 2.0, 2.0};
        System.out.println(Arrays.toString(array));
        int n = array.length;
        for (int i = 0, m = 0; i != n; i++, n = m) {
            for (int j = m = i + 1; j != n; j++) {
                if (array[j] != array[i]) {
                    if (m != j) array[m] = array[j];
                    m++;
                }
            }
        }
        if (n != array.length) {
            double[] b = new double[n];
            for (int i = 0; i < n; i++) b[i] = array[i];

            array = b;
        }

        for (double x : array) System.out.print(x + " ");
        System.out.println();
    }

    private static void individual2() {
        Random rand = new Random();
        Scanner in = new Scanner(System.in);
        double array[][] = new double[10][10];
        int col;
        double min, max;

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                array[i][j] = new BigDecimal(rand.nextDouble()).setScale(2, RoundingMode.HALF_UP).floatValue();
                System.out.format("%.2f ", array[i][j]);
            }
            System.out.println();
        }
        System.out.print("Choose a column (1-10) - ");
        col = (in.nextInt() - 1);

        min = max = array[0][col];
        for (int i = 0; i < 10; i++) {
            if (array[i][col] < min) min = array[i][col];
            if (array[i][col] > max) max = array[i][col];
        }
        System.out.format("Min = %.2f  Max = %.2f", min, max);
    }
}

