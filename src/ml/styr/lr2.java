package ml.styr;

import java.util.Scanner;

import static java.lang.Math.*;

public class lr2 {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int num, numLength, digitToChange;
        int i = 5, j = 2;
        double a, b, x, c;
        double result;
        String numBinary;

        System.out.println("tasks 1 & 2");

        System.out.print("Input number:  ");
        num = in.nextInt();
        numBinary = Integer.toBinaryString(num);
        System.out.println();
        System.out.println("Binary of " + num + " is: " + numBinary);
        numLength = Integer.toBinaryString(num).length() - 1;
        do {
            System.out.print("Choose digit to change from 0 to " + numLength + ": ");
            digitToChange = in.nextInt();
        } while (0 > digitToChange || digitToChange > numLength);
        StringBuilder sbNumBinary = new StringBuilder(numBinary);
        sbNumBinary.setCharAt(numLength - digitToChange, numBinary.charAt(numLength - digitToChange) == '0' ? '1' : '0'); //numBinary.charAt(numLength-digitToChange) == 0 ? "1" : "0" ;
        System.out.println("--New binary is: " + sbNumBinary);

        //task 3
        System.out.println("task 3");
        System.out.println("I = " + i + " J = " + j);
        i ^= j;
        i ^= j ^= i;
        System.out.println("I = " + i + " J = " + j);

        //individual
        System.out.println("individual");
        boolean userInput;
        x = 7.1;
        a = 0.001;
        b = 5;
        c = 4.1;

        System.out.println("Do you want to input values manually? (true/false)");
        userInput = in.nextBoolean();
        System.out.println(userInput);
        if (userInput) {
            System.out.println("Input value for x: ");
            x = in.nextDouble();
            System.out.println("Input value for a: ");
            a = in.nextDouble();
            System.out.println("Input value for b: ");
            b = in.nextDouble();
            System.out.println("Input value for c: ");
            c = in.nextDouble();
        }
        result = exp(-a * x) * cos(b * x + c) + exp(a * x) * sin(c * x - 1);
        System.out.println("Result is " + result);
    }
}
